(function () {
  'use strict';

  // Custom scripts
  document.addEventListener("DOMContentLoaded", function() {

    // Live Awesomplete Search 
  	var inputHero = document.getElementById("search-hero");
  	var inputNavbar = document.getElementById("search-navbar");

    var list = [
          { label: "Show search Result page", 					                value: "search_result.html" },
          { label: "How do I enable payment gateway?", 					        value: "search_result.html" },
          { label: "What happens if I clear cache?", 						        value: "search_result.html" },
          { label: "Which payment methods do you offer?", 				      value: "search_result.html" },
          { label: "How long does it take to propagate DNS settings?", 	value: "search_result.html" },
          { label: "How can I force SSL certificate?", 					        value: "search_result.html" }
        ];

    if (inputHero) {
      inputHero.addEventListener("awesomplete-selectcomplete", function(e) {
        window.location.href = e.text.value;
      }, false);
      
      new Awesomplete(inputHero, {
        autoFirst: true,
        list: list,
        replace: function(suggestion) {
          this.input.value = suggestion.label;
        }
      });
    }

    if (inputNavbar) {
      inputNavbar.addEventListener("awesomplete-selectcomplete", function(e) {
        window.location.href = e.text.value;
      }, false);
      
      new Awesomplete(inputNavbar, {
        autoFirst: true,
        list: list,
        replace: function(suggestion) {
          this.input.value = suggestion.label;
        }
      });
    }

  });

}());
